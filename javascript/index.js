var findForm = document.getElementsByTagName('form');
window.onload = function(){
	var date = document.querySelectorAll("footer p");
	date[0].innerHTML = "This page was last modified on: " + document.lastModified;
	
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
	
	$('#commentError').hide();
	findForm[0].onsubmit = validateForm;
};

function validateForm() {
	if (findForm[0].myExperience.value.length < 10) {
		var errorMessage = document.getElementById('commentError');
		errorMessage.innerHTML = "Please enter more experience";
		$('#commentError').show();
		return false;
	}
	return true;
};
